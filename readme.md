## API routes

### GET /api/radio-towers/
```json
[
    {
        "id": 1,
        "name": "Eska Rock Warszawa",
        "gps_location": "SRID=4326;POINT (21.1142799 52.2360421)",
        "location": "Jubilerska 10, 04-190 Warszawa"
    },
    {
        "id": 2,
        "name": "Eska Radom",
        "gps_location": "SRID=4326;POINT (21.1540861 51.400803)",
        "location": "Plac Konstytucji 3 Maja, Radom"
    },
    {
        "id": 3,
        "name": "Antyradio Katowice",
        "gps_location": "SRID=4326;POINT (19.0135237 50.2634556)",
        "location": "ul. Mickiewicza 29 40-085 Katowice"
    }
]
```

### POST /api/radio-towers/
#### Request
```json
{
    "name": "RMF MAXXXX",
    "location": "ul. Nowy Świat 50/3, 25-522 Kielce"
}
```

#### Response
```json
{
    "id": 4,
    "name": "RMF MAXXXX",
    "gps_location": "SRID=4326;POINT (20.630326 50.8767359)",
    "location": "ul. Nowy Świat 50/3, 25-522 Kielce"
}
```

### POST /api/search-by-location/
#### Request
```json
{
    "location": "Pionki"
}
```

#### Response
```json
{
    "id": 2,
    "name": "Eska Radom",
    "gps_location": "SRID=4326;POINT (21.1540861 51.400803)",
    "location": "Plac Konstytucji 3 Maja, Radom"
}
```

### POST /api/search-by-gps-location/
#### Request
```json
{
    "latitude": 52.2309,
    "longitude": 21.0571
}
```

#### Response
```json
{
    "id": 1,
    "name": "Eska Rock Warszawa",
    "gps_location": "SRID=4326;POINT (21.1142799 52.2360421)",
    "location": "Jubilerska 10, 04-190 Warszawa"
}
```