# Generated by Django 2.2.11 on 2020-05-25 16:15

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RadioTower',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('gps_location', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('location', models.CharField(max_length=50)),
            ],
        ),
    ]
