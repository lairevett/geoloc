from django.contrib import admin
from django.http import HttpResponseBadRequest
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.urls import path, reverse
from leaflet.admin import LeafletGeoAdmin

from .forms import SearchTowerForm
from .models import RadioTower


@admin.register(RadioTower)
class RadioTowerAdmin(LeafletGeoAdmin):

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path('search/', self.admin_site.admin_view(self.search_view), name='radio_tower_search')
        ]

        return custom_urls + urls

    def search_view(self, request):
        table_template_path = 'admin/radio_tower/partials/search_results_table.html'
        search_results_table = render_to_string(table_template_path)

        if request.method == 'POST':
            name = request.POST.get('name')
            if not name:
                return HttpResponseBadRequest()

            towers = RadioTower.objects.filter(name__icontains=name)
            search_results_table = render_to_string(table_template_path, {'results': towers})

        context = dict(
            self.admin_site.each_context(request),
            form_action=request.get_full_path(),
            form=SearchTowerForm(),
            api_request_url=f'{reverse("radiotower-list")}?search=',
            search_results_table=search_results_table
        )

        return TemplateResponse(request, 'admin/radio_tower/search.html', context)
