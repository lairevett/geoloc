from django.conf import settings
from django.contrib.gis.geos import Point
from rest_framework import serializers

from .models import RadioTower
from .utils import get_latitude_and_longitude


class RadioTowerSerializer(serializers.ModelSerializer):
    class Meta:
        model = RadioTower
        fields = '__all__'
        read_only_fields = ('gps_location',)

    def save(self, **kwargs):
        (latitude, longitude) = get_latitude_and_longitude(self.validated_data['location'])
        gps_location = Point(x=longitude, y=latitude, srid=settings.GIS_SRID)
        return super().save(gps_location=gps_location, **kwargs)
