import geocoder
from django.conf import settings
from django.contrib.gis.db.models.functions import Distance

from .models import RadioTower


def get_latitude_and_longitude(location):
    location_data = geocoder.google(location, key=settings.GEOCODER_API_KEY)
    return location_data.latlng


def get_nearest_tower(gps_location):
    return RadioTower.objects \
        .annotate(distance=Distance('gps_location', gps_location)) \
        .order_by('distance').first()
