from django import forms


class SearchTowerForm(forms.Form):
    name = forms.CharField(label='Nazwa stacji', max_length=50)
