from django.urls import path

from .views import search_by_location, search_by_gps_location

urlpatterns = [
    path('search-by-location/', search_by_location),
    path('search-by-gps-location/', search_by_gps_location)
]
