from django.conf import settings
from django.contrib.gis.geos import Point
from rest_framework import viewsets, permissions, mixins, filters
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.validators import ValidationError

from .models import RadioTower
from .serializer import RadioTowerSerializer
from .utils import get_latitude_and_longitude, get_nearest_tower


class RadioTowerViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    queryset = RadioTower.objects.all()
    serializer_class = RadioTowerSerializer
    search_fields = ['name']
    filter_backends = (filters.SearchFilter,)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def search_by_location(request):
    location = request.data.get('location')
    if not location:
        raise ValidationError('You have to specify the location of tower.')

    (latitude, longitude) = get_latitude_and_longitude(location)
    found_gps_location = Point(x=longitude, y=latitude, srid=settings.GIS_SRID)

    tower = get_nearest_tower(found_gps_location)
    serialized_tower = RadioTowerSerializer(tower)
    return Response(serialized_tower.data)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def search_by_gps_location(request):
    latitude = request.data.get('latitude')
    longitude = request.data.get('longitude')
    if not latitude or not longitude:
        raise ValidationError('You have to specify the latitude and longitude.')

    gps_location = Point(x=longitude, y=latitude, srid=settings.GIS_SRID)

    tower = get_nearest_tower(gps_location)
    serialized_tower = RadioTowerSerializer(tower)
    return Response(serialized_tower.data)
