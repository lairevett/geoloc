from django.conf import settings
from django.contrib.gis.db import models


class RadioTower(models.Model):
    name = models.CharField(max_length=50, unique=True, blank=False, null=False)
    gps_location = models.PointField(srid=settings.GIS_SRID)
    location = models.CharField(max_length=50, blank=False, null=False)

    def __str__(self):
        return self.name
