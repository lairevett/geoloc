import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'drbmuigx0dszr!*$t0qu(j9_wp6p=$fil_oy$(93&1d01g=htn'
DEBUG = True
ALLOWED_HOSTS = []
ROOT_URLCONF = 'app.urls'
WSGI_APPLICATION = 'app.wsgi.application'
STATIC_URL = '/static/'
GIS_SRID = 4326  # WGS 84 projection
GEOCODER_API_KEY = os.environ['GEOCODER_API_KEY']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'leaflet',
    'rest_framework',
    'radio_tower',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'geoloc',
        'USER': 'geoloc',
        'PASSWORD': os.environ['API_DBPASS'],
        'HOST': '192.168.33.10',
        'PORT': 5432
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ],
}

LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (48.8566, 2.3522),  # (latitude, longitude)
    'DEFAULT_ZOOM': 18,
    'MIN_ZOOM': 3,
    'MAX_ZOOM': 20,
    'SCALE': 'both',
    'RESET_VIEW': False,
    'TILES': [
        ('OpenStreetMap', 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
         {'attribution': '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'})
    ]
}

LANGUAGE_CODE = 'pl-pl'
TIME_ZONE = 'Europe/Warsaw'
USE_I18N = True
USE_L10N = True
USE_TZ = True
