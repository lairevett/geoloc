from django.contrib import admin
from django.urls import path, include
from radio_tower.views import RadioTowerViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'radio-towers', RadioTowerViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api/', include('radio_tower.urls')),
]
